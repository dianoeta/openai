import os, logging
from configparser import ConfigParser
from bottle import route, run, request, post, default_app
from llama_index import GPTVectorStoreIndex, LLMPredictor, ServiceContext, StorageContext
from llama_index import set_global_service_context, load_index_from_storage
from langchain.chat_models import ChatOpenAI

# set logging to info level to track token usage
# logging.basicConfig(level=logging.INFO)

# read config file
config = ConfigParser()
config.read(".config")
# set API Key
os.environ["OPENAI_API_KEY"] = config['APP']['KEY']

# define LLM and set service context
llm_predictor = LLMPredictor(llm=ChatOpenAI(temperature=0.6, model_name="gpt-3.5-turbo"))
service_context = ServiceContext.from_defaults(llm_predictor=llm_predictor)
set_global_service_context(service_context)

users = {"rosa": "Ms. Rosa", "rjlawns": "RJ Lawns"}
storage_context = {}
index = {}

# load context and vector index for all users #
for user in users:
    # rebuild storage context from vector store folder
    storage_context[user] = StorageContext.from_defaults(persist_dir=user)
    # load index
    index[user] = load_index_from_storage(storage_context[user])


@post('/<user>')
def user_response(user):

    if request.forms.review:
        # set query engine
        try:
            query_engine = index[user].as_query_engine()
            prompt = f"write a 50 word response from {users[user]} to the review: {request.forms.review}"
            response = query_engine.query(prompt)
            return({"response": str(response).strip()})
        except Exception as e:
            return({"error": str(e)})


application = default_app()


if __name__ == '__main__':
    run(application, host='0.0.0.0', port=8080, reloader=True, debug=True)
