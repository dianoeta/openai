## OpenAi-Langchain-llamaindex Playground ##

### Setup and install ###

Steps:

1. Create a python virtual environment

2. Activate the environment

3. Install dependencies

4. Add openai key to config file



### Scripts and their functions ###

* **ask_ai.py**:

* **ai_training.py**: Desktop application written in python-Tk to generate vector index folders from training data submitted as a folder containing documents or as web urls. The index folders thus generated are used by askai_api and ai_review_response scripts, thereby saving on repeated training tokens.


* **askai_api.py**:

* **ai_review_response.py**:

* **aindex.py**: module used to generate vector index folder from various sources

### llamaindex features tested ###


### Langchain features tested ###
