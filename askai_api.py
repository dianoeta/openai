import os, aindex, logging
from configparser import ConfigParser
from bottle import route, run, request, post, default_app, auth_basic
from llama_index import GPTVectorStoreIndex, LLMPredictor, ServiceContext, StorageContext
from llama_index import set_global_service_context, load_index_from_storage, download_loader
from langchain.chat_models import ChatOpenAI
from langchain import OpenAI

# set logging to info level to track token usage
logging.basicConfig(level=logging.INFO)

# set service context from config file
config = ConfigParser()
# read config file
config.read(".config")

# set API Key
os.environ["OPENAI_API_KEY"] = config['APP']['KEY']

# define LLM and set service context
# llm_predictor = LLMPredictor(llm=OpenAI(temperature=0.6, model_name="text-davinci-003"))

# switched to cheaper GPT-TURBO-3.5 model, to evaluate it wrt DaVinci-003
llm_predictor = LLMPredictor(llm=ChatOpenAI(temperature=0.6, model_name="gpt-3.5-turbo"))
service_context = ServiceContext.from_defaults(llm_predictor=llm_predictor)
set_global_service_context(service_context)


def ai_train():
    # rebuild storage context
    storage_context = StorageContext.from_defaults(persist_dir=config['APP']['INDEX'])
    # load index
    index = load_index_from_storage(storage_context)
    return(index)


index = ai_train()
logging.debug(index)


# web functions begin #
def is_salted(user, salt):
    if config['APP'].get('USER') == user and config['APP'].get('SALT') == salt:
        return True
    else:
        return False


@post('/')
@auth_basic(is_salted)
def ai_response():

    if request.forms.question:
        # set query engine
        try:
            query_engine = index.as_query_engine()
            response = query_engine.query(request.forms.question)
            source_nodes = [node.to_dict() for node in response.source_nodes]
            return({"response": str(response).strip(), "source_nodes": source_nodes})
        except AttributeError:
            return({"error": "Training data or API key Invalid"})

    else:
        return({"response": "The answer to everything is ... wait for it... 42"})


@post('/init')
@auth_basic(is_salted)
def app_init():
    if not (request.forms.user.strip() and request.forms.salt.strip() and request.forms.key.strip()):
        return({"Error": "user, salt and key required"})
    config['APP'] = {"USER": request.forms.user, "SALT": request.forms.salt, "KEY": request.forms.key}
    with open('.config', 'w') as configfile:
        config.write(configfile)
    os.environ["OPENAI_API_KEY"] = config['APP']['KEY']
    global index
    index = ai_train()
    if index:
        return({"Success": "Key verified"})
    else:
        return({"Error": "Key Verification failed"})


application = default_app()


if __name__ == '__main__':
    run(application, host='0.0.0.0', port=8080, reloader=True, debug=True)
