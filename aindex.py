""" module for preparing index from documents or webpages """
import os
from llama_index import GPTVectorStoreIndex, ServiceContext, SimpleDirectoryReader
from llama_index import download_loader


def folder(folder_name):
    """ use aindex.folder(<path to folder containing data>) """
    # read documents from folder
    documents = SimpleDirectoryReader(folder_name).load_data()
    return(__make_index__(documents))


def pages(urls):
    """ use aindex.pages(<list>[webpages]) """
    # load webpage downloader
    SimpleWebPageReader = download_loader("SimpleWebPageReader")
    loader = SimpleWebPageReader()
    documents = loader.load_data(urls=urls)
    return(__make_index__(documents))


def sites(urls):
    """ use aindex.pages(<list>[websites]) """
    # load Beautifulsoup website downloader
    BeautifulSoupWebReader = download_loader("BeautifulSoupWebReader")
    loader = BeautifulSoupWebReader()
    documents = loader.load_data(urls=urls)
    return(__make_index__(documents))


def __make_index__(documents):
    # prepare index #
    try:
        # for ada-002 chunk size of 512 work quite well,
        # optimal chunks reduce noise while preserving semantic relevance
        service_context = ServiceContext.from_defaults(chunk_size=512)
        index = GPTVectorStoreIndex.from_documents(documents, service_context=service_context)
        return(index)
    except Exception:
        return(None)
