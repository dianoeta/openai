import os, aindex, logging
from llama_index import GPTVectorStoreIndex, LLMPredictor, ServiceContext, StorageContext
from llama_index import set_global_service_context, load_index_from_storage
from llama_index.optimization.optimizer import SentenceEmbeddingOptimizer
from langchain.chat_models import ChatOpenAI
from langchain import OpenAI

# set logging to info level to track token usage
logging.basicConfig(level=logging.INFO)

# set API Key
api_key = input("Please enter your api key: ")
os.environ["OPENAI_API_KEY"] = api_key.strip()

# set NLTK_DATA env variable for NLTK data (grammar/models etc)
os.environ["NLTK_DATA"] = os.getcwd()+"/nltk_data"

# define LLM and set service context
# llm_predictor = LLMPredictor(llm=OpenAI(temperature=0.6, model_name="text-davinci-003"))

# switched to cheaper GPT-TURBO-3.5 model, to evaluate it wrt DaVinci-003
llm_predictor = LLMPredictor(llm=ChatOpenAI(temperature=0.6, model_name="gpt-3.5-turbo"))
service_context = ServiceContext.from_defaults(llm_predictor=llm_predictor)
set_global_service_context(service_context)


def ai_train(mode):
    if mode == "folder":
        # wait for data population in folder
        print("Please put all your training data (pdf/txt) files in a folder name training_data and press enter.")
        input()
        # build index
        index = aindex.folder('training_data')

    elif mode == "pages":
        # get urls from user
        urls = input("Please provide a comma separated list of urls you would like to fetch the data from: ")
        print("Fetching data from web. Please wait...")
        # build index
        urls = [url.strip() for url in urls.split(",")]
        index = aindex.pages(urls)

    elif mode == "site":
        # get site url from user
        url = input("Which site you would like to fetch the data from: ")
        print("Fetching data from web. Please wait...")
        # build index
        index = aindex.sites([url])

    else:
        # exit if faulty choice
        print("You are too dumb to use ai")
        exit()

    # store index
    index.storage_context.persist()
    print("Training completed")


def ai_chat():
    # rebuild storage context
    storage_context = StorageContext.from_defaults(persist_dir="./storage")
    # load index
    index = load_index_from_storage(storage_context)
    # set query engine
    # query_engine = index.as_query_engine()

    # using sentence embedding optimisations to filter off low relevance sentence units
    query_engine = index.as_query_engine(optimizer=SentenceEmbeddingOptimizer(percentile_cutoff=0.5))
    question = input("Query: ")
    while question != 'end':
        response = query_engine.query(question)
        print('The following nodes were used for the response: ')
        for node in [srcnode.to_dict() for srcnode in response.source_nodes]:
            print("\n", node, "\n")
        print("\nResponse: ", response, "\n")

        question = input("Query: ")


training_required = input("Do you wish to train the ai with new/updated data (y/n): ")
if training_required == "y":
    mode = input('''which of the following modes of training would uoi like to use:
        folder: Train ai on your text and pdf documents placed in the training folder
        pages:  Train ai on webpage urls supplied as comma separated values
        site:   Train ai on the data scraped from a website

    Please enter training mode: ''')
    ai_train(mode.strip())

print("Answer 'end' at the query prompt to exit.")
ai_chat()
