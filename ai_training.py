""" desktop app to create GPTVectorIndex from local folder or web data """

import os, logging, aindex
from tkinter import Tk, Label, Entry, Button, StringVar, filedialog, LabelFrame, Radiobutton, END
# from tkinter.ttk import LabelFrame
from tkinter.scrolledtext import ScrolledText
from configparser import ConfigParser
from pathlib import Path

# set logging to info level to track token usage
logging.basicConfig(level=logging.INFO)

## initialize app ##
root = Tk()
training_folder = StringVar()
training_mode = StringVar(value="folder")
status = StringVar()

# set service context from config file
config = ConfigParser()
# read config file
config.read(".config")

# set API Key
os.environ["OPENAI_API_KEY"] = config['APP']['KEY']


## Defining functions ##
def vector_index():
    """ generate and save vector index """
    if not idx_name.get():
        status.set("Index name is required to create the vector index folder.")
        return

    status.set("Processing data and creating index might take some time. Please wait..")
    root.update_idletasks()
    if training_mode.get() == "folder":
        vindex = aindex.folder(training_folder.get())
    else:
        # convert url list to array
        urls = txt_urls.get('1.0', END).split()
        logging.info(urls)
        vindex = getattr(aindex, training_mode.get())(urls)

    if vindex:
        # save the generated vector index to a folder
        vindex.storage_context.persist(persist_dir=idx_name.get())
        status.set("process complete. Index files saved in folder " + idx_name.get())
    else:
        status.set("Index creation failed. Kindly check urls and key and try again")
    return


def set_training_folder():
    training_folder.set(filedialog.askdirectory())


## Defining gui elements ##
root.configure()
root.title("Ai Training")
# set_theme()

# textbox for index name
idx_name = Entry(root)
Label(root, text="Index Name:\n(kindly do not use spaces or special characters)").pack(pady=5, padx=5)
idx_name.pack(expand=True)

# Training mode slection radio buttons
lf = LabelFrame(root, text='Training Mode')
Radiobutton(lf, text="Folder", value="folder", variable=training_mode).grid(row=0, column=0)
Radiobutton(lf, text="Web Pages", value="pages", variable=training_mode).grid(row=0, column=1)
Radiobutton(lf, text="Web Sites", value="sites", variable=training_mode).grid(row=0, column=2)
lf.pack(pady=10)

# Local training folder selection
Button(root, text="Choose source folder", command=set_training_folder).pack()
Entry(root, textvariable=training_folder, width=40, state='readonly').pack(padx=10)

# url list for web training
Label(root, text="List of urls for training. (Only one url per line in the format https://xyz.com)").pack(pady=5)
txt_urls = ScrolledText(root, width=60, height=20)
txt_urls.pack(padx=5)
# Button to start index preparation
Button(root, text="Generate Vector Index", command=vector_index).pack(pady=5)
Label(root, textvariable=status).pack()

root.mainloop()
